# snapfile.vue

## TODO

- add CI for snapfile.spring (add .jar to docker build context fails)
- make docker auto restart with the lastest img
- make code more complicated, with letters
- improve write & deploy experience: docker jenkins
- add analytic tools, like umeng.inc
- improve download & upload speed (move to ucloud oss)
- resp use JSON
- think about frontend & backend api

## Done

- add Jenkins, push commits can auto build images now
- send to v2ex, first public show: https://www.v2ex.com/t/395230#reply6
- better(shorter) domain name: snapfileX.com
- rename file to code-originalFilename.filetype
- improve UI, have Header now
- add domin, make it run

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Docker

see https://bitbucket.org/snapfile-inc/snapfile.spring#markdown-header-docker
